from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from importer.reddit import RedditImporter
from importer.newsapi import NewsApiImporter


class NewsViewSet(viewsets.ViewSet):
    """Showing news which aggregated from reddit and newsapi"""
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        query = self.request.query_params.get('query', None)
        if query:
            reddit = RedditImporter('news', query).search()
            news_api = NewsApiImporter(query=query).search()
        else:
            reddit = RedditImporter('news', query).get_all_posts()
            news_api = NewsApiImporter(query=query).get_all_posts()

        result = []
        if reddit:
            result = [*reddit]

        if news_api:
            result = [*result, *news_api]

        return Response(result)
