import os
import requests
import urllib.parse

from .base import BaseImporter


class NewsApiImporter(BaseImporter):
    """
    Handle gettig data from newsapi.org
    Sample endpoint requests:
        ('http://newsapi.org/v2/top-headlines?'
        'category=genreral&'
        'sortBy=popularity&'
        'q=Apple&'
        'apiKey=ba780f8ff82e4180875b788d8a54c8e7)
    """

    def __init__(self, category='general', query=None):
        self.BASE_URL = 'http://newsapi.org/v2/top-headlines/'
        self.category = category
        self.query = query
        self.api_key = os.environ.get('NEWS_API_KEY')

    def parse_data(self, results):
        """
        We need to parse returned json from our request to news api. Here you
        can see a sample of returned json:
        {
            "status": "ok",
            "totalResults": 1818,
            "articles": [
                {
                    "source": {
                        "id": null,
                        "name": "Lifehacker.com"
                    },
                    "author": "David Murphy",
                    "title": "Why Can't My Smart Speakers Stream Music Correctly?",
                    "url": "https://lifehacker.com/why-cant-my-smart-speakers-stream-music-correctly-1843181102",
                }
            ]
        }
        """
        parsed_result = []
        data = results.get('articles')
        for child in data:
            if child:
                parsed_result.append(
                    {
                        'headline': child.get('title'),
                        'link': child.get('url'),
                        'source': 'newsapi'
                    }
                )

        return parsed_result

    def get_all_posts(self):
        url = urllib.parse.urljoin(
            self.BASE_URL,
            (f'?category={self.category}&'
            'sortBy=popularity&'
            f'apiKey={self.api_key}'))
        return self.make_request(url)

    def search(self):
        url = urllib.parse.urljoin(
            self.BASE_URL,
            (f'?category={self.category}&'
            'sortBy=popularity&'
            f'q={self.query}&'
            f'apiKey={self.api_key}'))
        return self.make_request(url)
