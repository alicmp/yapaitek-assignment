import os
import requests
import urllib.parse


class BaseImporter:
    """
    Handle gettig data 
    """

    def make_request(self, url, headers=None):
        """This method is for DRY (dont repeat yourself) sake"""
        if headers:
            response = requests.get(url, headers=headers)
        else:
            response = requests.get(url)

        if response.ok:
            results = response.json()
            return self.parse_data(results)
        return None
