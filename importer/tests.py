from django.test import TestCase
from unittest.mock import Mock, patch

from .reddit import RedditImporter
from .newsapi import NewsApiImporter


class ImporterTest(TestCase):

    @patch('importer.reddit.requests.get')
    def test_reddit(self, mock_get):
        sample_data = {
            "data": {
                "children": [
                    {
                        "data": {
                            "title": "Cryptocurrency market value jumps $35 billion in 24 hours led by a surge in bitcoin",
                            "url": "http://test.com"
                        }
                    }
                ]
            }
        }

        # Config mock data
        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = sample_data

        importer = RedditImporter(subreddit='news', query='bitcoin')
        all_posts = importer.get_all_posts()
        self.assertNotEqual(all_posts, None)

        searched_posts = importer.search()
        self.assertNotEqual(searched_posts, None)

    @patch('importer.reddit.requests.get')
    def test_reddit_fail(self, mock_get):
        sample_data = None

        # Config mock data
        mock_get.return_value = Mock(ok=False)
        mock_get.return_value.json.return_value = sample_data

        importer = RedditImporter(subreddit='news', query='bitcoin')
        all_posts = importer.get_all_posts()
        self.assertEqual(all_posts, None)

    @patch('importer.newsapi.requests.get')
    def test_news_api(self, mock_get):
        sample_data = {
            "status": "ok",
            "totalResults": 1818,
            "articles": [
                {
                    "source": {
                        "id": 1,
                        "name": "Lifehacker.com"
                    },
                    "author": "David Murphy",
                    "title": "Why Can't My Smart Speakers Stream Music Correctly?",
                    "url": "https://lifehacker.com/why-cant-my-smart-speakers-stream-music-correctly-1843181102",
                }
            ]
        }

        # Config mock data
        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = sample_data

        importer = NewsApiImporter(query='bitcoin')
        all_posts = importer.get_all_posts()
        self.assertNotEqual(all_posts, None)

        searched_posts = importer.search()
        self.assertNotEqual(searched_posts, None)
    
    @patch('importer.newsapi.requests.get')
    def test_news_api_fail(self, mock_get):
        sample_data = None

        # Config mock data
        mock_get.return_value = Mock(ok=False)
        mock_get.return_value.json.return_value = sample_data

        importer = NewsApiImporter(query='bitcoin')
        all_posts = importer.get_all_posts()
        self.assertEqual(all_posts, None)