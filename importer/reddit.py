import os
import requests
import requests.auth
import urllib.parse

from .base import BaseImporter


class RedditImporter(BaseImporter):
    """
    Handle gettig data from reddit
    Sample endpoint requests:
        https://www.oauth.reddit.com/r/news/search.json?q=bitcoin&restrict_sr=on
        https://oauth.reddit.com/r/news/new.json
    """

    def __init__(self, subreddit, query=None):
        self.BASE_URL = 'http://oauth.reddit.com/r/'
        self.subreddit = subreddit
        self.query = query
        self.headers = {"User-Agent": "AssignmentYapaiTek/1.1 by PeculiarIrony"}
        self.get_token()
    
    def get_token(self):
        client_id = os.environ.get('REDDIT_CLIENT_ID')
        client_secret = os.environ.get('REDDIT_CLIENT_SECRET')
        username = os.environ.get('REDDIT_USERNAME')
        password = os.environ.get('REDDIT_PASSWORD')
        if client_id and client_secret and username and password:
            client_auth = requests.auth.HTTPBasicAuth(client_id, client_secret)
            post_data = {"grant_type": "password", 
                         "username": username,
                         "password": password}
            response = requests.post(
                "https://www.reddit.com/api/v1/access_token", auth=client_auth,
                data=post_data, headers=self.headers)
            self.token = response.json().get('access_token')
            self.headers['Authorization'] = f"bearer {self.token}"
        else:
            self.BASE_URL = 'http://reddit.com/r/'

    def parse_data(self, results):
        """
        We need to parse returned json from our request to reddig. Here you can
        see a sample of returned json:
        {
            "data": {
                "children": [
                    {
                        "data": {
                            "title": "some title",
                            "url": "http://test.com"
                            ...
                        }
                    }
                ]
            }
        }
        """
        parsed_result = []
        data = results.get('data')
        if not data:
            return None

        children = data.get('children')
        for child in children:
            if child.get('data'):
                parsed_result.append(
                    {
                        'headline': child.get('data').get('title'),
                        'link': child.get('data').get('url'),
                        'source': 'reddit'
                    }
                )

        return parsed_result

    def get_all_posts(self):
        url = urllib.parse.urljoin(
            self.BASE_URL,
            f'{self.subreddit}/new.json'
        )
        return self.make_request(url, self.headers)

    def search(self):
        url = urllib.parse.urljoin(
            self.BASE_URL,
            f'{self.subreddit}/search.json?q={self.query}&restrict_sr=on'
        )
        return self.make_request(url, self.headers)