FROM python:3.7

ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
RUN pip install pipenv


RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pipenv install --system --deploy --ignore-pipfile