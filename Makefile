
#
# YapAiTek assignment
# @file
# @version 0.1

makemigrations:
	docker-compose run --rm web sh -c "python manage.py makemigrations"

migrate:
	docker-compose run --rm web sh -c "python manage.py migrate"

createsuperuser:
	docker-compose run --rm web sh -c "python manage.py createsuperuser"

run_test:
	docker-compose run --rm web sh -c "python manage.py test"

# end
