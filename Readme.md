# YapAiTek assignment
## Description

For this assignment you have to implement an application that aggregates news from two different APIs. The APIs you'll be using are Reddit and News API. This application should be running on your localhost and serve the result in JSON format from an endpoint whenever it gets a request. The two functionalities that need to be implemented are "list" and "search".
## Requirements
- Docker!

## Setup
- First you need to get your newsapi.org api key, check out `https://newsapi.org/` and register, then get your api key.
- After that navigate to projects root directory and create `.env` file. You need to put api key which you got in previous step into this file. Check `.env.sample`!
- (Optional) Then you need to get reddit client id and client secret, visit `https://www.reddit.com/prefs/apps` and create new app with whatevern name you want, choose `script` and create the app. Then head back to `.env` file and put your client id and secret as well as your reddits username and password in it. Check `.env.sample` to see how is the format! Although this part is optional but if you make multiple requests reddit block you and its better to go through this part if you want to use this project extensivly.
- Build docker container
```
docker-compose up --build
```
- After that you have to create a user
```
make createsuperuser
```
- Voila! Thats it, you can use to project now.

## Endpoints
- `/api/v1/account/token/` You have to get users token which you created in setup part. In order to get token make a post request into this url, and put your username and password in request body.
- `/api/v1/news` If you want to get news you have to make a request into this endpoint. and if you want to filter news you simply add `?query=YOUR_QUERY` to this url. You have to put the token you got in previous step into header with this format: `Authorization: token YOUR_TOKEN`

## Tasks
- [x] Create django project
- [x] Reddit importer
- [x] News API importer
- [x] Unittest for importer
- [x] Use DRF for creating endpoint
- [x] Authentication
- [x] Dockerize the project
- [x] Complete readme

## Test
In order to run unittests simply execute this command:
```
make run_test
```

## Improvements
- It is a good practice to seperate settings for production and development environments but since this is simple test i thought it is not necessary.

## Heads Up
- For the first time that you want to build docker container you may get this error: `ERROR: could not find an available, non-overlapping IPv4 address pool among the defaults to assign to the network`. If this happened just turn of your vpn then you are good to go. This problem only appears for the first time that you build the project after that you can turn on your vpn. Although it depends on your operation system and your vpn client and its likely you dont have any problems. 
- When i was testing the project i occasionaly got 403 error whenever i wanted to made request to reddit. But turning on the VPN is fixing this problem.
